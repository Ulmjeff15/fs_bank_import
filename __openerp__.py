# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (c) 2011 OpenERP S.A. <http://openerp.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Framasoft Import Bank Statements',
    'version': '1.0',
    'category': 'Tools',
    'description': """
Provides an interface to import the bank statements.
====================================================

Add an interface to import the bank statements of the following bank accounts:
    * PayPal
    * Crédit Mutuel / CIC
    * Crédit Coopératif

    """,
    'author': 'Framasoft',
    'website': 'http://www.framasoft.org',
    'depends': [
        'account',
    ],
    'data' : [
    ],
    'icon': '/fs_bank_import/static/src/img/fs_bank_import.png',
    'test': [
    ],
    'qweb': [
    ],
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
